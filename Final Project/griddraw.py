from random import randint

#################
#GRID ASCII
#################

def drawTile(graph, id, style, width):
    r = "."
    if 'number' in style and id in style['number']: r = "%d" % style['number'][id]
    if 'start' in style and id == style['start']: r = "S"
    if 'end' in style and id == style['end']: r = "E"
    if id in graph.walls: r = "#" * width
    #forest objects
    if graph == graphForest:
        r = "🌿  "
        if id in graph.cliffs: r = "🧱"
        if id in graph.house: r = "🏘️  "
    # desert objects
    if graph == graphDesert:
        r = "🟧  "
        if id in graph.radioactive: r = "☢️  "
    #path
    if 'path' in style and id in style['path']: r = "🀄"
    return r

def drawGrid(graph, width=2, **style):
    for y in range(graph.height):
        for x in range(graph.width):
            print("%%-%ds" % width % drawTile(graph, (x, y), style, width), end="")
        print()

#################
#GRID ARRAY
#################

class SquareGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
        self.cliffs = []
        self.house = []
        self.radioactive = []
        self.radNeighbors = []
        self.houseNeighbors = []
    
    def bounds(self, id):
        (x, y) = id
        return 0 <= x < self.width and 0 <= y < self.height
    
    def passable(self, id):
        return id not in (self.walls or self.cliffs or self.radioactive or self.radNeighbors)
    
    def neighbors(self, id):
        (x, y) = id
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        results = filter(self.passable, filter(self.bounds, results))
        return results

class GridWithWeights(SquareGrid):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.weights = {}
    
    def cost(self, startNode, endNode):
        return self.weights.get(endNode, 1)

#################
#GRAPHS
#################

#################
#default
#################

graph = GridWithWeights(20, 20)
# graph.walls = [(1, 7), (1, 86), (2, 7), (2, 8), (3, 7), (3, 8)]
locX = [randint(0, 19) for x in range(0, 100)]
locY = [randint(0, 19) for y in range(0, 100)]

graph.weights = {weight: randint(0, 5) for weight in list(zip(locX, locY))}

#################
#Forest
#################
graphForest = GridWithWeights(20, 20)

graphForest.cliffs = [(3, 4), (4, 4), (4, 5), (5, 4), (5, 5), (6, 4), (6, 5), (7, 4), (7, 5), (8, 3), (8, 4), (9, 3), (10, 3), (4, 8), (4, 9), (5, 8), (5, 9), (6, 8), (6, 9), (6, 7), (7, 7), (8, 7), (9, 7), (9, 8), (9, 9), (10, 8), (10, 9), (10, 10), (11, 10), (11, 11), (12, 10), (12, 11), (13, 10), (13, 11), (13, 12), (13, 13), (14, 12), (15, 12), (16, 11), (12, 15), (11, 15), (11, 16), (10, 17), (10, 18), (9, 19), (8, 19), (7, 19), (6, 19), (12, 16), (13, 17), (6,6)]

graphForest.house = [(2,18), (2,17), (3,17), (4,17), (4,18), (17,3), (18,3), (16,3), (17,4), (16,4), (18,4)]

temp = [(15,3), (14,3), (13,3), (12,3), (11,3), (4,10), (4,11), (4,12), (4,13), (4,14), (4,15), (4,16)]

for i in range(0, len(graphForest.house)-1):
    (x, y) = graphForest.house[i]
    graphForest.houseNeighbors.append((x+1, y))
    graphForest.houseNeighbors.append((x, y-1))
    graphForest.houseNeighbors.append((x-1, y))
    graphForest.houseNeighbors.append((x, y+1))
    graphForest.houseNeighbors.append((x+1, y+1))
    graphForest.houseNeighbors.append((x-1, y-1))
    graphForest.houseNeighbors.append((x+1, y-1))
    graphForest.houseNeighbors.append((x-1, y+1))

forestWeightX = [randint(0, 19) for x in range(0, 100)]
forestWeightY = [randint(0, 19) for y in range(0, 100)]

weightCoords = list(zip(forestWeightX, forestWeightY))

graphForest.houseNeighbors = list(set(graphForest.houseNeighbors))

trueWeights = [x for x in weightCoords if x not in (graphForest.house and graphForest.houseNeighbors)]

graphForest.weights = {**{weight: 1 for weight in (graphForest.house and graphForest.houseNeighbors)}, **{weight: randint(10,20) for weight in trueWeights}, **{weight: randint(500,1000) for weight in temp}}

#################
#Desert
#################
graphDesert = GridWithWeights(20, 20)

radweightX = [randint(0, 19) for x in range(0, 30)]
radweightY = [randint(0, 19) for y in range(0, 30)]

radCoords = list(zip(radweightX, radweightY))

#create radneighbours out of radioactive items
for i in range(0, len(radCoords)-1):
    (x, y) = radCoords[i]
    graphDesert.radNeighbors.append((x+1, y))
    graphDesert.radNeighbors.append((x, y-1))
    graphDesert.radNeighbors.append((x-1, y))
    graphDesert.radNeighbors.append((x, y+1))

#remove duplicates
graphDesert.radioactive = list(set(radCoords))
graphDesert.radNeighbors = list(set(graphDesert.radNeighbors))
#set weights for graph
graphDesert.weights = {**{location: randint(100,200) for location in (graphDesert.radioactive and graphDesert.radNeighbors)}, **{location: 9001 for location in (graphDesert.radioactive and graphDesert.radNeighbors)}}
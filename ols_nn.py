
#!/usr/bin/env python3
# ols_nn.py                                                   SSimmons Oct. 2018
"""
Uses the nn library along with auto-differentiation to define and train, using
batch gradient descent, a neural net that finds the ordinary least-squares
regression plane. The data are mean centered and normalized.  The plane obtained
via gradient descent is compared with the one obtained by solving the normal
equations.
"""
import csv
import torch
import torch.nn as nn

# Read in the data.
with open('temp_co2_data.csv') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  next(csvfile)  # skip the first line of csvfile
  xss, yss = [], []
  for row in reader:
    xss.append([float(row[2]), float(row[3])])
    yss.append([float(row[1])])

# The tensors xss and yss now containing the features (i.e., inputs) and targets
# (outputs) of the data.
xss, yss = torch.tensor(xss), torch.tensor(yss)

# For validation, compute the least-squares regression plane using linear alg.
try:
  lin_alg_sol = torch.lstsq(yss, torch.cat((torch.ones(len(xss),1), xss), 1))
except AttributeError:
  lin_alg_sol = torch.gels(yss, torch.cat((torch.ones(len(xss),1), xss), 1))

# Compute the column-wise means and standard deviations.
xss_means, yss_means = xss.mean(0), yss.mean()
#xss_stds, yss_stds  = xss.std(0), yss.std()

# Mean center and normalize
xss, yss = xss - xss_means, yss - yss_means
#xss, yss = xss/xss_stds, yss/yss_stds

# Build the model
class LinearRegressionModel(nn.Module):

  def __init__(self):
    super(LinearRegressionModel, self).__init__()
    self.layer = nn.Linear(2,1)

  def forward(self, xss):
    #Every time forward is called, layer is set to xss
    return self.layer(xss)

# Create an instance of the above class.
model = LinearRegressionModel()
#print("The model is:\n", model)
#print("xss is: \n", xss)
#print("The range of xss: \n", xss.size())
for param in model.parameters():
  print(param)

# Set the criterion to be mean-squared error
criterion = nn.MSELoss()

learning_rate = 0.000526
momentum = 0.914
epochs = 10000

numExamples = len(xss)
batch_size = 32
z_params = []
for param in model.parameters():
    z_params.append(param.data.clone())
for param in z_params:
    param.zero_()
print(z_params)


#print("Range: ", range(0,numExamples,batch_size))

for epoch in range(epochs):  # train the model
  # yss_pred refers to the outputs predicted by the model
  indices = torch.randperm(numExamples)
  totalLoss=0
  for i in range(0,numExamples,batch_size):
    #print("index start: ",i)
    #print("index + batch_size:",i+batch_size)
    #print("Indexed xss: ",xss[i:i+batch_size,:])
    #print("Indexed yss: ", yss[i:i+batch_size,:])
    current_indices = indices[i:i+batch_size]
    yss_pred = model(xss.index_select(0, current_indices))
    #print("yss_pred: ",yss_pred)
    loss = criterion(yss_pred, yss.index_select(0,current_indices)) 
    totalLoss+=loss.item()


    #print("epoch: {0}, current loss: {1}".format(epoch+1, loss.item()))

    model.zero_grad() # set the gradient to the zero vector
    loss.backward() # compute the gradient of the loss function w/r to the weights

    #adjust the weights
    for i,(z,param) in enumerate(zip(z_params, model.parameters())):
      z_params[i] = momentum*z + param.grad.data
      param.data.sub_(learning_rate*z_params[i])

  print("Total Loss for epoch ",epoch,": ",totalLoss*batch_size/numExamples)

# extract the weights and bias into a list
params = list(model.parameters())

if not ('xss_means' in locals() and 'yss_means' in locals()):
    xss_means = torch.Tensor([0.,0.])
    yss_means = 0.0
    print('\nno centering')
if not ('xss_stds' in locals() and 'yss_stds' in locals()):
    xss_stds = torch.Tensor([1.,1.])
    yss_stds = 1.0
    print('no normalization')
# Un-mean-center and un-normalize.
w = torch.zeros(3)
w[1:] = params[0] * yss_stds / xss_stds
w[0] = params[1].data.item() * yss_stds + yss_means - w[1:] @ xss_means

print("The least-squares regression plane:")
# Print out the equation of the plane found by the neural net.
print("  found by the neural net is: "+"y = {0:.3f} + {1:.3f}*x1 + {2:.3f}*x2"\
    .format(w[0],w[1],w[2]))

# Print out the eq. of the plane found using closed-form linear alg. solution.
print("  using linear algebra:       y = "+"{0:.3f} + {1:.3f}*x1 + {2:.3f}*x2"\
.format(lin_alg_sol[0][0][0], lin_alg_sol[0][1][0], lin_alg_sol[0][2][0]))
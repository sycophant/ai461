'''Make a board for Wumpus World
See Russel and Norvig chapter 7
Dr. Browning January 31,2020'''
#board col and row are numbered 0-3 not 1-4.
#Player starts in 0,0 in upper left corner.

import random

DIMENSION = 4
PIT_PROB_FACTOR = 5 #= 100/%.  So 1 in 5 squares are pits - 20% probability

def randomLocation():
    row = random.randint(0,DIMENSION-1)
    col = random.randint(0,DIMENSION-1)
    return col,row

class Square:
    def __init__(self,row,col):
        self.row = row
        self.col = col
        #These are declared here and set in the Board
        self.player = False 
        self.wumpus, self.stench = False,False
        self.pit, self.breeze = False, False
        self.gold,self.glitter = False,False 
    def getInfo(self): #Used to print the board
        answer = ''
        if self.player: answer += "player "
        if self.wumpus: answer += "wumpus "
        #1. add the information for pit, gold, stench, and breeze
        if self.pit: answer += "pit "
        if self.gold: answer += "gold "
        if self.stench: answer += "stench "
        if self.breeze: answer += "breeze "

        return answer
        
class Board:
    def __init__(self):
        #make the board
        self.squares = []
        for row in range(DIMENSION):
            thisRow = []
            for col in range(DIMENSION):
                thisRow.append(Square(col,row))
            self.squares.append(thisRow)
 
        #place the player in the starting square
        self.player = (0,0)
        self.squares[0][0].player = True
        #place the wumpus somewhere else
        self.wumpusCol,self.wumpusRow = randomLocation()
        #2. Make sure the wumpus is not in the player's starting square
        while ((self.wumpusCol,self.wumpusRow) == self.player):
            self.wumpusCol,self.wumpusRow = randomLocation()
        self.squares[self.wumpusCol][self.wumpusRow].wumpus = True
        
        #randomly place pits 
        for col in range(DIMENSION):
            for row in range(DIMENSION):
                current = self.squares[col][row]
                if random.randint(1,PIT_PROB_FACTOR)==1:
                #20% of the time if prob factor is 5
                    current.pit = True
        #3. place gold and glitter
        self.gold = (0,0)
        self.squares[0][0].gold = True
        self.goldCol,self.goldRow = randomLocation()
        while ((self.goldCol,self.goldRow) == self.player):
            self.goldCol,self.goldRow = randomLocation()
        self.squares[self.goldCol][self.goldRow].gold = True
        self.squares[self.goldCol][self.goldRow].glitter = True

        #place stenches and breezes
        for col in range(DIMENSION):
            for row in range(DIMENSION):
                current = self.squares[col][row]
                checkthese=[]
                if row > 0:checkthese.append(self.squares[col][row-1])
                if row < DIMENSION-1: checkthese.append(self.squares[col][row+1])
                if col > 0:checkthese.append(self.squares[col-1][row])
                if col < DIMENSION-1: checkthese.append(self.squares[col+1][row])
                for check in checkthese:
                    if check.wumpus:current.stench = True
                    if check.pit:current.breeze = True

    def printBoard(self):
        for row in range(DIMENSION):
            out = ''
            for col in range(DIMENSION):
                square = self.squares[col][row]
                out=out+square.getInfo()+"** "
            print(out)

def main(): #4. generate and print 5 boards
    for i in range(0, 5):
        myBoard = Board()
        myBoard.printBoard()
        print('-----------------------------')
        i += 1
    #print()

main()

#5. On your paper, analyze each of your boards.  Is it possible to win? 
#Is it possible to logically figure out exactly how to win with this board?

player breeze ** pit breeze ** breeze ** **
gold stench breeze ** pit breeze ** breeze ** **
wumpus pit breeze ** pit stench breeze ** breeze ** **
stench breeze ** breeze ** ** **
-----------------------------

#Has to take a chance, 50% that it will die.


player breeze ** pit ** breeze ** **
** stench breeze ** ** breeze **
stench breeze ** wumpus pit ** stench breeze ** pit **
pit ** stench breeze ** pit gold ** breeze **
-----------------------------

# Very remote possibility. Has to take a chance at first square before any learning's happened
# and continue to take chances without falling into pits.


player pit ** breeze ** ** **
gold breeze ** ** breeze ** **
stench ** breeze ** pit ** breeze **
wumpus ** stench ** breeze ** pit **
-----------------------------

#Unwinnable.


player ** ** ** breeze **
** ** breeze ** pit stench **
** ** stench ** wumpus breeze **
gold ** ** breeze ** pit stench **
-----------------------------

#Straight shot to gold and back.

player ** ** breeze ** pit **
** breeze ** pit breeze ** breeze **
** gold stench breeze ** pit breeze ** breeze **
stench ** wumpus ** stench breeze ** **
-----------------------------

#Logically possible, clear run to gold doable and also when backtracking from breezy square.


# 2. Read pages 234-236 and define 
#     a. Knowledge base: Set of sentences that define models of possible worlds
#     b. Sentence: Assertion that describes a world
#     c. Axiom: self-evident sentence
#     d. Inference: Act of deriving new sentence from sentences already in kb


# 3. Read section 2.3 and section 7.2.  Is Wumpus world 
#     a. Fully or partially observable : partially
#     b. Single agent or multiagent: Single agent
#     c. Episodic or sequential: episodic
#     d. Static or dynamic: Static
#     e. Discrete or continuous: Discrete
'''Dijkstra's algorithm
by Carol Browning, January 26, 2020'''

#Describe the matrix with a list of vertex labels and an adjacency matrix
V=["Atlanta","Boston","Chicago","Denver","El Paso"]
#node travel cost
adjMatrix=[[0,1076,720,1406,1417],[1076,0,985,1983,2397],[720,985,0,1006,1513],[1406,1983,1006,0,635],[1417,2397,1513,635,0]]
#direct travel
crowDistances = [[0,936,589,1210,1289],[936,0,849,1765,2068],[589,849,0,918,1251],[1210,1765,918,0,557],[1289,2068,1251,557,0]]
#Set infinity to a number that is larger than any path cost.  This is
#the total if you went across every edge twice.
inf = sum([sum(a) for a in adjMatrix])#way bigger than any path cost.  

    #Note: for larger problems use a priority queue to maintain the incomplete list

class Vertex:
    def __init__(self,label,pathCost,pathParent):
        self.label = label # used for printing the paths
        self.pathCost = pathCost # lowest path cost seen so far
        self.estimate = est #cost of direct travel
        self.pathParent = pathParent # parent on path of lowest cost so far
        self.index = 0 # used for finding edge costs in adjacency matrix
        self.heuristic = self.pathCost + self. estimate #heristic for A*
    def setPathcost(self, value):
        self.pathCost = value
        self.heuristic = self.pathCost + self. estimate
    def setEstimate(self, value)
        self.estimate = value
        self.heuristic = self.pathCost + self. estimate

def smallest(vlist): # finds the vertex in vlist with the smallest pathCost
    #sets initial no first node in the vertex list
    initV = vlist[0]
    #for all th vertexes, does comparison checkes against the initial node's pathcost. If better, takes that node as the best solution.
    for v in range(1, len(vlist)):
      if vlist[v].heuristic < initV.heuristic: initV = vlist[v]:
        initV = vlist[v]
    #returns the node with the best solution.
    return initV

def call(sVert, eVert):
    vertexlist = [Vertex(vertex,inf,None) for vertex in V]
    for i in range(len(vertexlist)): vertexlist[i].index = i
    # #cost to get to A from A is 0
    # vertexlist[0].pathCost = 0
    # #say A is the parent of itself to indicate starting node
    # vertexlist[0].pathParent = vertexlist[0]
    # #we'll collect the description of the path in answer
    # answer = []
    # vertnode = vertexlist[0]
    # #assign node user is looking for
    for v in vertexlist:
      if v.label == sVert:
        vertStart = v
    temp = vertStart
    vertexlist[vertStart.index] = vertexlist[0]
    vertexlist[0] = temp
    for v in vertexlist:
      if v.label == eVert:
        vertEnd = v
    
    for i in range(len(vertexlist)):vertexlist[i].setH(crowDistances[i][vertEnd.index])

    vertexlist[0].setG(0)

    vertexlist[0].pathParent = vertexlist[0]

    


    #incompleteList is a list of nodes for which shortest path is not yet determined
    incompleteList = vertexlist.copy()
    while incompleteList != []:
        #Find the uncompleted vertex with smallest path cost.  
        bestV = smallest(incompleteList)
        #We now know the shortest path to bestV
        answer = answer + [(bestV.label,"through",bestV.pathParent.label,"with cost",bestV.pathCost)]
        #so bestV is completed
        incompleteList.remove(bestV)
        #Is the path through bestV to any uncompleted vertices better than what we know so far?
        for vertex in incompleteList:
            edgeCost = adjMatrix[bestV.index][vertex.index]
            if edgeCost != 0: #there is an edge from bestV to the vertex
                if bestV.pathCost + edgeCost < vertex.pathCost: #we found an improvement
                    vertex.pathCost = bestV.pathCost + edgeCost
                    vertex.pathParent = bestV
        #after node is finalized, compare against user requested vertex and break.
        if bestV.label == vertnode.label: break
    print(answer)
        
def main():
  sVert = input('Please input start vertex: ')
  eVert = input('Please input end vertex: ')
  call(sVert, eVert)
'''A* algorithm
by Carol Browning, January 27, 2020'''

#Describe the graph with a list of vertex labels and an adjacency matrix
V=["Atlanta","Boston","Chicago","Denver","El Paso"]

#adjMatrix=[[0,1076,720,1406,1417],[1076,0,985,1983,2397],[720,985,0, 1006,1513],[1406,1983,1006,0,635],[1417,2397,1513,635,0]]
# with direct driving route from Atlanta to El Paso removed and Boston to El Paso removed:
adjMatrix=[[0,1076,720,1406,0],[1076,0,985,1983,0],[720,985,0, 1006,1513],[1406,1983,1006,0,635],[0,0,1513,635,0]]

crowDistances = [[0,936,589,1210,1289],[936,0,849,1765,2068],[589,849,0, 918,1251],[1210,1765,918,0,557],[1289,2068,1251,557,0]]

#Set infinity to a number that is larger than any path cost.  This is
#the total if you went across every edge twice.
inf = sum([sum(a) for a in adjMatrix])#way bigger than any path cost.  
print("inf =", inf)
    #Note: for larger problems use a priority queue to maintain the incomplete list

class Vertex:
    def __init__(self,label,G=inf,pathParent=None,H=0):
        self.label = label # used for printing the paths
        self.pathParent = pathParent # parent on path of lowest cost so far
        self.index = 0 # used for finding edge costs in adjacency matrix
        self.G = G # lowest path cost seen so far
        self.H = H # will be crow distance to goal city
        self.F = G+H # A* heuristic
    def setG(self,value):
        self.G = value
        self.F = self.G+self.H
    def setH(self,value):
        self.H = value
        self.F = self.G+self.H      

def smallest(vlist): # finds the vertex in vlist with the smallest heuristic F
    smallest = vlist[0]
    for index in range(1,len(vlist)):
        if vlist[index].F < smallest.F: smallest = vlist[index]
    return smallest

def printAnswer(vertexlist,startVertex,endVertex):
    answer = "The shortest path from "+startVertex.label+" to "+endVertex.label
    answer = answer +" has length "+str(endVertex.F )+":"
    print(answer)
    citylist = [endVertex.label]
    cityVertex = endVertex
    while cityVertex != startVertex:
        cityVertex = cityVertex.pathParent
        citylist = [cityVertex.label] + citylist
    print(citylist)

def main():
    vertexlist = [Vertex(vertex) for vertex in V]
    for i in range(len(vertexlist)): vertexlist[i].index = i # set the indices for adjacency matrix
    
    #Get start city and end city
    startString = input("Choose a starting city: ")
    endString = input("Choose an ending city: ")
    #find the starting vertex and move it to the front of vertexlist
    for vertex in vertexlist:
        if vertex.label == startString:
            startVertex = vertex
    #move starting vertex to front of vertexlist        
    temp = startVertex
    vertexlist[startVertex.index] = vertexlist[0]
    vertexlist[0] = temp
    #find the ending vertex    
    for vertex in vertexlist:
        if vertex.label == endString:
            endVertex = vertex
            
    #set the H values as crow distances from each vertex to the end vertex city
    for i in range(len(vertexlist)): vertexlist[i].setH(crowDistances[i][endVertex.index])
    
    #cost to get to A from A is 0
    vertexlist[0].setG(0)

    #say A is the parent of itself to indicate starting node
    vertexlist[0].pathParent = vertexlist[0]
    
    #incompleteList is a list of nodes for which shortest path is not yet determined
    incompleteList = vertexlist.copy()
    done = False
    while incompleteList != [] and not done:
        #Find the uncompleted vertex with smallest path cost.  
        bestV = smallest(incompleteList)
        #We now know the shortest path to bestV
        if bestV.label == endString:
            printAnswer(vertexlist, startVertex,endVertex)
            done = True
        #answer = answer + [(bestV.label,"through",bestV.pathParent.label,"with cost",bestV.G)]
        else: #so bestV is completed
            incompleteList.remove(bestV)
            #Is the path through bestV to any uncompleted vertices better than what we know so far?
            for vertex in incompleteList:
                edgeCost = adjMatrix[bestV.index][vertex.index]
                if edgeCost != 0: #there is an edge from bestV to the vertex
                    if bestV.G + edgeCost < vertex.G: #we found an improvement
                        vertex.setG(bestV.G + edgeCost)
                        vertex.pathParent = bestV
                       

main()

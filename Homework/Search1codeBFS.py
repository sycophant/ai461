"""
Solution to the Peasant, cabbage, goat, and wolf river crossing problem
By Carol Browning
January 20, 2020
"""

""" Define constants """

east = 1   #to change sides, multiply current side by -1
west = -1
p=0   #state list will be a list of sides (east or west) in the order
c=1   #peasant, cabbage, goat, wolf
g=2
w=3

""" Define the Node class"""

class Node:
    def __init__(self,state,parent,action,cost):
        self.state = state   # a state is a list of four values, each either east or west
        self.parent = parent # this is the pointer to the previous state
        self.action = action # what action is performed on parent to get this state
        self.cost = cost     # length of path from initial state to this one

""" Given a state, generate a list of the possible actions """

#an action is a list of items to move.  
def actionList(state): 
    currentSide = state[p]
    mylist = [[p]] 
    for item in [c,g,w]:
        if state[item] == currentSide:
            mylist.append([p,item])
    return(mylist)

""" Check to see whether or not the given state is valid.  Goats eat cabbage and wolves eat goats """

def isValid(state):
    if state[p] != state[g]:
        if state[g] == state[c] or state[g] ==state[w]: return False
    return True

""" Given a current state and an action, determine the next state """

def makeNewState(currentState,action):
    if action == []: return currentState
    newState = []
    for x in [p,c, g, w]:
        if x in action:
            newState.append(-1*currentState[x])  #This is why we defined east and west to be 1 and -1
        else:
            newState.append(currentState[x])
    return newState

""" findAnswer is the main algorithm for searching the space for a solution """

def findAnswer():
    initial = [west,west,west,west]
    goal = [east,east,east,east]
    initialNode = Node(initial,None,None,0)
    frontierNodes = [initialNode]
    exploredStates = []

    #For each node in the frontier
    while frontierNodes != [ ]:
        current = frontierNodes.pop() #remove the node from the frontier
        #print(current.state)
        if current.state == goal:   #Check to see if we've reached the goal
            return(current,len(exploredStates))
        actions = actionList(current.state) #Make a list of the actions we can perform in this state
        #add current node to explored
        exploredStates.append(current.state)    #Add this state to the explored states list
        
        #for each action, create a new node if it is valid and not already seen
        for action in actions:
            newState = makeNewState(current.state,action)
            if isValid(newState):
                newNode = Node(newState,current,action,current.cost+1)
                if (newNode not in frontierNodes) and (newNode.state not in exploredStates):
                    frontierNodes.append(newNode)
                    
        #uncomment the above print statement and the next line to step through the solution    
        #mystring=input("press return to continue")

    #if frontier is empty and no solution has been found
    print("No solution found") 
    return(None)

""" Print the path from the initial node to the given node """

def printAnswer(node):
    #if parent is None, we are done.  Otherwise, print the previous step then print this one.
    if node.parent != None:

        #print the path to the previous step
        printAnswer(node.parent)

        #now print the action to get to this step
        message = " move peasant from "
        if node.state[0]==east:message = message + "west to east"
        else: message = message + "east to west"
        if len(node.action)>1:
            message = message + ", along with the "
            if node.action[1]==1:message = message +"cabbage"
            if node.action[1] == 2: message = message + "goat"
            if node.action[1] == 3: message = message + "wolf"
        print(node.action,node.state, message)
        
    else: #We have recursed back to the initial state
        print(node.state, " peasant, cabbage, goat, and wolf are on the west bank")

""" Main program. Find the answer and print the answer and the analysis """

def main():
    finalNode,numExploredStates = findAnswer()
    printAnswer(finalNode)
    print("Number of trips = ",finalNode.cost)
    print("Number of explored states = ",numExploredStates)


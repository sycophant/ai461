'''A* algorithm for finding a path through a grid
by Dr. Browning, January 28, 2020

In an n by n grid, some walls are erected, specified in the grid with 1's.          
Given a starting position and ending position (valid i.e. not in walls)
find the shortest path from start to end.'''

# GRID = [[0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 1, 1, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0]]

# GRID = [[0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0]]

# GRID = [[0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 1, 1, 1, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 1, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0]]

# GRID = [[0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 1, 1, 1, 0, 1, 1, 0],
#         [0, 1, 0, 1, 0, 1, 0, 0],
#         [0, 1, 0, 1, 0, 1, 1, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0]]

GRID = [[0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 1, 1, 0, 1, 0],
        [0, 0, 0, 1, 0, 0, 1, 0],
        [0, 0, 0, 1, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0]]
N = len(GRID) # dimension - GRID is N by N
NODEC = 0

START = (1,6)
END = (7,1)
INF = N*N # a number larger than the length of a path between any two places in the grid

'''
grid = [[0, 0,     0, 0, 0, 0, 1, 0],
        [0, 0,     0, 0, 0, 0, 1, END],
        [0, 0,     0, 0, 0, 0, 1, 0],
        [0, 0,     0, 1, 0, 1, 1, 0],
        [0, 0,     0, 1, 0, 0, 0, 0],
        [0, 0,     0, 1, 0, 0, 0, 0],
        [0, START, 0, 1, 0, 0, 0, 0],
        [0, 0,     0, 1, 0, 0, 0, 0]]

We can move up, down, left, or right as long as we don't move onto a wall (1) or off the grid.
'''
MOVES = [(0,-1),(0,1),(-1,0),(1,0),(1,-1),(1,1),(-1,-1),(-1,1)]
'''
g is the number of moves taken to get to a location.
h is the square of the Euclidean distance from our location to end.
'''

#Each grid location visited on our search will have a Vertex object
class Vertex:
    def __init__(self,column, row,G=INF,pathParent=None):
        self.column = column # location in grid
        self.row = row
        self.pathParent = pathParent # parent on path of lowest cost so far
        self.G = G # cost of getting here
        self.H = (column - END[0])**2 + (row - END[1])**2
                # square of Euclidean distance to end
        self.F = G+self.H # A* heuristic
    def setG(self,value):
        self.G = value
        self.F = self.G + self.H
        #set the value of G and update F
        #Assignment 1
    def printVertex(vertex):
        printV = "vertex: (" + str(vertex.column) + "," + str(vertex.row) + ")"
        print(printV)

    def samePlace(self,anotherVertex):
        #Tell whether or not two vertices refer to the same grid location
        if self.column == anotherVertex.column and self.row == anotherVertex.row:
            return True
        return False

def smallest(vlist): # finds the vertex in vlist with the smallest heuristic value F
    #Assignment 2
    firstV = vlist[0]
    for v in vlist:
        if v.F <= firstV.F:
            firstV = v
    return firstV

#Print the answer - the path from startVertex to endVertex 
def printAnswer(startVertex,endVertex):
    #See Assignment 5
    answer = "The shortest path from ("+str(startVertex.column)+","+str(startVertex.row)+") to ("+str(endVertex.column)+","+str(endVertex.row)+")"
    answer = answer +" has length "+str(endVertex.F )+":"
    print(answer)
    citylist = ["("+str(endVertex.column)+","+str(endVertex.row)+")"]
    cityVertex = endVertex
    finalGrid = GRID
    while cityVertex != startVertex:
        finalGrid[cityVertex.row][cityVertex.column] = 9
        cityVertex = cityVertex.pathParent
        citylist = ["("+str(cityVertex.column)+","+str(cityVertex.row)+"), "] + citylist
    print(citylist)
    print('')
    print("9 marks the path taken in the grid.")
    print('')
    for gridline in finalGrid: print(gridline)

#See if a vertex refers to the same grid position as a node on a list of vertices.  If so, return that node.
def onList(vertex, vlist):
    #if vertex's position is same as position of a node on vlist return that node
    for node in vlist:
        #Assignment 3
        if vertex.row == node.row and vertex.column == node.column:
            return node
    #else return None
    return None

#See if a given move from a given vertex should be added to the openList or not.  If so, add it.
#If it is already on the open list, update its G value.
def considerMoving(move,bestV,closedList,openList):
    global NODEC
    newColumn = bestV.column + move[0]
    newRow = bestV.row + move[1]
    if newColumn >= 0 and newColumn < N and newRow >= 0 and newRow < N :#new position is on grid
        if GRID[newRow][newColumn] != 1: #new position is not a wall - notice each entry of Grid is a row so index by row then column
            #make vertex for new position
            newVertex = Vertex(newColumn,newRow,bestV.G+1,bestV)
            #if we haven't already closed this vertex
            if onList(newVertex,closedList)==None:
                nodeOnOpenList = onList(newVertex,openList)
                if nodeOnOpenList == None:
                    #add the vertex to the open list
                    openList.append(newVertex)
                    NODEC += 1
                    #Assignment 4
                else:
                    #update G value and path parent for the already open node 
                    nodeOnOpenList.G = newVertex.G
                    nodeOnOpenList.pathParent = newVertex
                    #Assignment 4

#Search the grid for a path from the start point to the end point, going around walls if necessary.
def main():
    GRID[0][0] = 8
    print(GRID)
    #make the starting vertex
    startVertex = Vertex(START[0],START [1],0)
    startVertex.pathParent = startVertex
    startVertex.printVertex()

    #We will generate vertices as we encounter them.  When a new vertex is generated as we search, we add it to the open list.
    openList = [startVertex]
    #We will move vertices to the closed list as we search past them
    closedList = []

    #open list is a list of vertices we need to explore
    done = False
    while openList != [] and not done:
        #Find the next vertex to explore: open list element with smallest F.  
        bestV = smallest(openList)
        if bestV.column == END[0] and bestV.row == END[1]:
            printAnswer(startVertex,bestV)
            done = True
        else: #so bestV is completed
            openList.remove(bestV)
            closedList.append(bestV)
            #look at the moves we can make from bestV.  Which ones get added to openList?
            for move in MOVES:
                considerMoving(move,bestV,closedList,openList)
    print("-----------------------------")
    print(str(NODEC) + " was the number of times a node was added.")
                       
main()
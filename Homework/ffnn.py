import numpy as np

#######################
#1
#######################

# Build a feed-forward neural network with two inputs, two perceptrons in a single hidden
# layer, two perceptrons in the output layer such that the top output is the carry bit
# and the bottom output is the sum bit for performing base 2 addition of the two input bits.


inputs=np.array([[1,1,0,0],[1,1,0,1]]) 
#preset weights
weights=[np.array([1,1,0,0]),np.array([1,1,0,1])]

#activation
def check(n):
    if n<0: return 0
    else: return n

for arr in inputs :

    #hidden layers
    layer=check((arr*weights[0]).sum()) + check((arr*weights[0]).sum())

    #outputs
    out=check(([layer]*weights[1]).sum() + ([layer]*weights[1]).sum())

    print(arr,out)


#######################
#2
#######################

# Build a perceptron with n inputs that returns 1 if all the 0 inputs come before all the 1
# inputs for the case where
#     a) n = 2			(1 for 00, 01, 11 and 0 for 10)
#     b) n = 3

#a
inputs = np.array([[0,0],[0,1],[1,1],[1,0]])
weights=[np.array([0,0]),np.array([0,-1]),np.array([-1,-1]),np.array([-1,5])]
#b
# inputs = np.array([[0,0,0],[0,0,1],[0,1,0],[0,1,1],[1,0,0],[1,0,1],[1,1,0]])
# weights=[np.array([0,0,0]),np.array([0,0,-1]),np.array([0,-1,5]),np.array([5,-1,-1]),np.array([-1,5,5]),np.array([-1,5,-1]),np.array([-1,-1,5])]

def percep(n):
    if n<=0 : return 1
    else : return 0

out = 1

for i in inputs:
	for j in range(0, len(inputs)):
		if (percep((i*weights[j]).sum())) >= 1:
			out = 0

print(out)